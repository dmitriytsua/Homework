button = document.getElementById("btn")
btn.onclick = async function() {
    const ip = await fetch("https://api.ipify.org?format=json");
    const IntProt = await ip.json();
    const loc = await fetch(`http://ip-api.com/json/${IntProt.ip}?lang=ru&fields=continent,country,region,city,district`);
    const location = await loc.json();
    const continent = document.createElement("h1")
    continent.innerHTML = location.continent
    const country = document.createElement("h2")
    country.innerHTML = location.country
    const region = document.createElement("h3")
    region.innerHTML = location.region
    const city = document.createElement("h4")
    city.innerHTML = location.city
    const district = document.createElement("h5")
    district.innerHTML = location.district
    document.body.append(continent, country, region, city, district)
}
