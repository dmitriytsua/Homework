class Employee {
    constructor(name,age,salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    set name(name){
        this._name = name;
    }
    get name(){
        return this._name;
    }
    set age(age){
        this._age = age;
    }
    get age(){
        return this._age;
    }
    set salary(salary){
        this._salary = salary;
    }
    get salary(){
        return this._salary;
    }
}
class Programmer extends  Employee{
    constructor(name,age,salary,lang) {
        super(name,age,salary);
        this.lang = lang;
    }
    get salary() {
        return this._salary;
    }
    set salary(salary){
        this._salary = salary*3;
    }
}
const workman1 = new Programmer("Valera","30","10000",["Python"])
const workman2 = new Programmer("Nikita","23","12000",["CSS,HTML,JS"])
const workman3 = new Programmer("Kirill","45","15000",["C, C+,C++"])
console.log(workman1,workman2,workman3);