import React from 'react';
import './Modal.scss'

const Modal = (props) => {
	const {backgroundColor,headerColor,header, text, actions, handleClose} = props
	return (
		<div data-testid="background" className='modal__background'
			 onClick={(event) => event.currentTarget === event.target && handleClose()}>
			<div data-testid="content" style={{background: backgroundColor}} className='modal'>
				<div style={{background: headerColor}} className='modal__header'>
					<h5 data-testid='header'>{header}</h5>
					<div data-testid='buttonClose' className='modal__close' onClick={handleClose}/>
				</div>
				<div className='modal__content'>
					<p data-testid='text' className={"modal__text"}>{text}</p>
				</div>
				<div data-testid='actions' className='modal__actions'>
					{actions}
				</div>
			</div>
		</div>
	)
}

export default Modal;