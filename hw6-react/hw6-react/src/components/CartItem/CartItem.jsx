import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {openRemoveFromCartModal} from "../../store/action/ModalActions";
import {addToFavorites, removeFromFavorites} from "../../store/action/FavoritesActions";
import {decrementItemCountInCart, incrementItemCountInCart} from "../../store/action/CartActions";
import Button from "../Button/Button";
import StarIcon from "../../Icons/StarIcon";
import "./CartItem.scss"

const CartItem = ({item}) => {

	const dispatch = useDispatch()
	const cart = useSelector(store => store.cart)
	const favorites = useSelector(store => store.favorites)

	const {img, title, article, color, price} = item

	const count = cart.filter(i => i.article === item.article).length

	const isFavorite = () => favorites.some(i => i.article === article)

	const handleFavorite = () => {
		isFavorite()
			? dispatch(removeFromFavorites(item))
			: dispatch(addToFavorites(item))
	}

	return (
		<div className='cart-item'>
			<div className='cart-item__img-wrapper'>
				<img className='cart-item__img' src={img} alt={title}/>
			</div>
			<div className='cart-item__content'>
				<h5 className='cart-item__title'>{title}</h5>
				<p className='cart-item__article'>Article: {article}</p>
				<p className='cart-item__color'>Color: {color}</p>
			</div>
			<div className='cart-item__actions'>
				<div>
					<button className='cart-item__count-change-btn' onClick={() => dispatch(decrementItemCountInCart(item))}>
						-
					</button>
					<span className='cart-item__count'>{count}</span>
					<button className='cart-item__count-change-btn' onClick={() => dispatch(incrementItemCountInCart(item))}>
						+
					</button>
				</div>
				<span className='cart-item__price'>{price * count} $</span>
				<Button backgroundColor={"red"} text='Remove' onClick={() => dispatch(openRemoveFromCartModal(item))}/>
				<span className='cart-item__actions__fav' onClick={() => handleFavorite()}>
					{StarIcon(isFavorite() ? '#ffc107' : '#ffffff')}
				</span>
			</div>
		</div>
	)
}

export default CartItem;