import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {openAddToCartModal} from "../../store/action/ModalActions";
import {addToFavorites, removeFromFavorites} from "../../store/action/FavoritesActions";
import Button from "../Button/Button";
import StarIcon from "../../Icons/StarIcon";
import "./ItemCard.scss"

const ItemCard = ({item}) => {

	const {img, title, article, color, price} = item

	const dispatch = useDispatch()
	const favorites = useSelector(store => store.favorites)

	const isFavorite = () => favorites.some(i => i.article === article)

	const handleFavorite = () => {
		isFavorite()
			? dispatch(removeFromFavorites(item))
			: dispatch(addToFavorites(item))
	}

	return (
		<div className='card'>
			<div className='card__img-wrapper'>
				<img className='card__img' src={img} alt={title}/>
			</div>
			<div className='card__content'>
				<h5 className='card__title'>{title}</h5>
				<p className='card__article'>Article: {article}</p>
				<p className='card__color'>Color: {color}</p>
			</div>
			<div className='card__footer'>
				<span className='card__price'>{price} $</span>
				<div className='card__actions'>
					<Button backgroundColor={"green"} text='Add to cart' onClick={() => dispatch(openAddToCartModal(item))}/>
					<span className='card__actions__fav' onClick={() => handleFavorite()}>
						{StarIcon(isFavorite() ? '#ffc107' : '#ffffff')}
					</span>
				</div>
			</div>
		</div>
	)
}

export default ItemCard;