import React from 'react';
import {Link} from "react-router-dom";
import CartIcon from "../../Icons/CartIcon";
import './Header.scss'

const Header = (props) => {
	return (
		<header>
			<div className='container'>
				<nav className='navbar'>
					<a className='navbar__logo '  href="/">Very Bad Shop</a>
					<menu className='navbar__menu'>
						<li className='navbar__menu'>
							<Link className={"navbar__menu--link"} to='/'>Main</Link>
						</li>
						<li className='navbar__menu'>
							<Link className={"navbar__menu--link"} to='/favorites'>Favorites</Link>
						</li>
						<li className='navbar__menu'>
							<Link className={"navbar__menu--link"} to='/cart'>
								Cart
								<CartIcon/>
							</Link>
						</li>
					</menu>
				</nav>
			</div>
		</header>
	);
}

export default Header;