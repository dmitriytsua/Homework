import {fromLocalStorage, toLocalStorage} from "../../utils/LocalStorage";

export const LOAD_FAVORITES = "LOAD_FAVORITES"
export const ADD_TO_FAVORITES = "ADD_TO_FAVORITES"
export const REMOVE_FROM_FAVORITES = "REMOVE_FROM_FAVORITES"

export const loadFavorites = () => dispatch => {
	const items = fromLocalStorage('favorites', [])
	dispatch({
		type: LOAD_FAVORITES,
		items
	})
}

export const addToFavorites = item => dispatch => {
	const items = fromLocalStorage('favorites', [])
	items.push(item)
	toLocalStorage('favorites', items)
	dispatch({
		type: ADD_TO_FAVORITES,
		items
	})
}

export const removeFromFavorites = item => dispatch => {
	let items = fromLocalStorage('favorites', [])
	items = items.filter(i => i.article !== item.article)
	toLocalStorage('favorites', items)
	dispatch({
		type: REMOVE_FROM_FAVORITES,
		items
	})
}