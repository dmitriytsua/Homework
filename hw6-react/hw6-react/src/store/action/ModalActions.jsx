import React from "react";
import Modal from "../../components/Modal/Modal";
import Button from "../../components/Button/Button";
import {addToCart, removeFromCart} from "./CartActions";

export const OPEN_MODAL = "OPEN_MODAL"
export const CLOSE_MODAL = "CLOSE_MODAL"

export const openAddToCartModal = item => dispatch => {
	dispatch({
		type: OPEN_MODAL,
		modal: <Modal
			backgroundColor={"#4d4d4d"}
			headerColor={"#4d4949"}
			header='Add to cart'
			text='Do you want add this item to the cart?'
			closeButton={false}
			actions={<div>
				<Button backgroundColor={"#407c49"} text='Submit' onClick={() => {
					dispatch(addToCart(item))
					dispatch(closeModal())
				}}/>
				<Button backgroundColor={"#f22e21"} text='Cancel' onClick={() => dispatch(closeModal())}/>
			</div>}
			handleClose={() => dispatch(closeModal())}
		/>
	})
}

export const openRemoveFromCartModal = item => dispatch => {
	dispatch({
		type: OPEN_MODAL,
		modal: <Modal
			backgroundColor={"#e34234"}
			headerColor={"#e34234"}
			header='Remove from cart'
			text='Do you want remove this item from the cart?'
			closeButton={false}
			actions={<div>
				<Button backgroundColor={"#407c49"} text='Submit' onClick={() => {
					dispatch(removeFromCart(item))
					dispatch(closeModal())
				}}/>
				<Button backgroundColor={"gray"} text='Cancel' onClick={() => dispatch(closeModal())}/>
			</div>}
			handleClose={() => dispatch(closeModal())}
		/>
	})
}

export const closeModal = () => dispatch => {
	dispatch({type: CLOSE_MODAL})
}