import {
	ADD_TO_CART,
	CART_LOADING, CLEAN_CART,
	DECR_COUNT_IN_CART,
	INCR_COUNT_IN_CART,
	REMOVE_FROM_CART
} from "../action/CartActions";
import initStore from "../InitStore";

const cartReducer = (store = initStore.cart, {type, cart}) => {
	switch (type) {
		case CART_LOADING:
			return cart
		case ADD_TO_CART:
			return cart
		case REMOVE_FROM_CART:
			return cart
		case INCR_COUNT_IN_CART:
			return cart
		case DECR_COUNT_IN_CART:
			return cart
		case CLEAN_CART:
			return []
		default:
			return store
	}
}

export default cartReducer
