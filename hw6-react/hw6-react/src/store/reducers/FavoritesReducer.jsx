import {ADD_TO_FAVORITES, LOAD_FAVORITES, REMOVE_FROM_FAVORITES} from "../action/FavoritesActions";
import initStore from "../InitStore";

const favoritesReducer = (store = initStore.favorites, {type, items}) => {
	switch (type) {
		case LOAD_FAVORITES:
			return items
		case ADD_TO_FAVORITES:
			return items
		case REMOVE_FROM_FAVORITES:
			return items
		default:
			return store
	}
}

export default favoritesReducer