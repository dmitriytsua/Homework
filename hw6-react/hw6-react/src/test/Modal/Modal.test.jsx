import React from "react";
import {fireEvent, render} from "@testing-library/react";
import Modal from "../../components/Modal/Modal";
import Button from "../../components/Button/Button";

describe('Modal test', function (){

    test('Header test', function (){
        const {getByTestId} = render(<Modal header={'Header modal test'}/>)
        expect(getByTestId('header').tagName).toBe('H5')
        expect(getByTestId('header').textContent).toBe('Header modal test')
    })

test('Text test', function (){
    const {getByTestId}=render(<Modal text={'Text modal test'}/>)
    expect(getByTestId('text').tagName).toBe('P')
    expect(getByTestId('text').textContent).toBe('Text modal test')
})

    test('Close button test', function (){
        const {getByTestId}=render(<Modal closeButton={true}/>)
        expect(getByTestId('buttonClose')).not.toBe(null)
        expect(getByTestId('buttonClose').tagName).toBe('DIV')
        expect(getByTestId('buttonClose').className).toBe('modal__close')
    })

    test('Actions test', function() {
    const actions = <Button text={'Success'}/>
        const {getByTestId} = render(<Modal actions={actions}/>)
        expect(getByTestId('actions').children).toHaveLength(1)
        expect(getByTestId('actions').children[0].tagName).toBe('BUTTON')
        expect(getByTestId('actions').children[0].textContent).toBe('Success')
    })

    test('Handle background close test', function() {
    const closeModal = jest.fn()
const {getByTestId}=render(<Modal handleClose={closeModal}/>)
        fireEvent.click(getByTestId('background'))
        expect(closeModal).toHaveBeenCalledTimes(1)
    })
    test('Handle content close test', function() {
        const closeModal = jest.fn()
        const {getByTestId}=render(<Modal handleClose={closeModal}/>)
        fireEvent.click(getByTestId('content'))
        expect(closeModal).toHaveBeenCalledTimes(0)
    })
    test('Handle button close test', function() {
        const closeModal = jest.fn()
        const {getByTestId}=render(<Modal closeButton={true} handleClose={closeModal}/>)
        fireEvent.click(getByTestId('buttonClose'))
        expect(closeModal).toHaveBeenCalledTimes(1)
    })

})