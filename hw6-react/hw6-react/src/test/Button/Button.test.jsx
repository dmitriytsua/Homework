import React from 'react';
import {fireEvent, render} from "@testing-library/react";
import Button from "../../components/Button/Button";


describe('Button component test', function (){


    test('Text test', function (){
        const {getByTestId} = render(<Button text='Test text Hello'/>)
        expect(getByTestId('button').textContent).toBe('Test text Hello')
    })
    test('Background test', function (){
        const {getByTestId} = render(<Button backgroundColor={"orange"}/>)
        expect(getByTestId('button').style.backgroundColor).toBe('orange')
    })
    test('Click test', function (){
        const click = jest.fn()
        const {getByTestId} = render(<Button onClick={click}/>)
        fireEvent.click(getByTestId('button'))
        expect(click).toHaveBeenCalledTimes(1)
    })
})


