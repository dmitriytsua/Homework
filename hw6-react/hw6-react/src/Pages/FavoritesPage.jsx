import React from 'react';
import {useSelector} from "react-redux";
import ItemsContainer from "../components/ItemsContainer/ItemsContainer";

const FavoritesPage = () => {

	const items = useSelector(store => store.favorites)

	return (
		<main>
			<div className="container">
				<ItemsContainer items={items}/>
			</div>
		</main>
	)
}

export default FavoritesPage