import React from 'react';
import CartContainer from "../components/CartContainer/CartContainer";

const CartPage = () => {

	return (
		<div>
			<div className="container">
				<CartContainer/>
			</div>
		</div>
	)
}

export default CartPage