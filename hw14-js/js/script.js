$(window).scroll(function() {
    if ($(window).scrollTop() > 500) {
        $('#button').addClass('show');
    } else {
        $('#button').removeClass('show');
    }
});
$('#button').on('click', function(e) {
    $('html, body').animate({scrollTop:0}, '3000');
});


$("#home").one('click', function() {
    $('html,body').animate({
            scrollTop: 0}, '3000');
});
$("#posts").one('click', function() {
    $('html,body').animate({
        scrollTop: 500}, '3000');
});
$("#clients").one('click', function() {
    $('html,body').animate({
        scrollTop: 1500}, '3000');
});
$("#rated").one('click', function() {
    $('html,body').animate({
        scrollTop: 2000}, '3000');
});
$("#news").one('click', function() {
    $('html,body').animate({
        scrollTop: 2450}, '3000');
});
$( ".hide-rated" ).click(function(){
    $( ".top-rated" ).slideToggle();
});
