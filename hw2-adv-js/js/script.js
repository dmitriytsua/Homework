const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const divRoot = document.createElement('div');
divRoot.id = "root";
document.body.append(divRoot);
const ul = document.createElement('ul');
divRoot.append(ul);
books.forEach((obj, ind) => {
    const li = document.createElement("li");
    try{ if (!obj.author) {
            throw new SyntaxError(`Error! Not an autor ${ind}`);}
        if (!obj.price) {
            throw new SyntaxError(`Error! No price found ${ind}`);}
        if (!obj.name) {
            throw new SyntaxError(`Error! No name found ${ind}`);}
        li.innerHTML = `${ind}) author:${obj.author}, price:${obj.price}, name:${obj.name}`;
        ul.append(li);
    } catch(error){
        console.log(error.message);}});





