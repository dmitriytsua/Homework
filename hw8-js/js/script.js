document.getElementById("price").onblur = function () {
    let tempPrice = +price.value;
    price.innerText = tempPrice;
    if (tempPrice < 0) {
        document.getElementById('count').innerText = `Неправильное число`;
        document.getElementById('price').classList.add('line');
    } else {
        document.getElementById('price').classList.remove('line');
        document.getElementById('count').innerText = ``;
        let spans = document.getElementById('spans');
        let spanNode = document.createElement('span');
        spanNode.setAttribute('onclick', `removeSpan(this);`);
        let textNode = document.createTextNode(`Текущая цена: ${tempPrice}`);
        spanNode.appendChild(textNode);
        spans.appendChild(spanNode);
        console.log(tempPrice);
    }
};

let removeSpan = function(span) {
    span.parentNode.removeChild(span);
};

