const link = 'https://swapi.dev/api/films/';
fetch(link)
.then((response) => response.json())
.then((info) => {
    info.results.forEach((wars) => {
        wars.characters.forEach((character) => {
            const charactersP = document.createElement("p");
            fetch(character)
                .then((response) => response.json())
                .then(info => {
                    characters.append(info.name)
                    characters.append(charactersP)
                    })})
         const bodyDiv = document.createElement("div")
         document.body.append(bodyDiv)
         const info = document.createElement("p")
         const characters = document.createElement("div")
         const episodes = document.createElement("h2")
         const episodeTitle = document.createElement("h3")
         episodeTitle.append(`${wars.title}`)
         episodes.append("Episode number " + `${wars.episode_id}`)
         info.append(`${wars.opening_crawl}`)
         bodyDiv.append(episodes, episodeTitle, info)
         bodyDiv.append(characters)
    })})

