import React from 'react';
import ItemCard from "../ItemCard/ItemCard";
import "./ItemsContainer.scss"

const ItemsContainer = ({items}) => {

	const itemCards = items.map(
		(item, index) => <ItemCard key={(index + 1) * item.id * Math.random()} item={item}/>
	)

	return (
		<>
			{itemCards.length > 0
				? <div className='items-container'>{itemCards}</div>
				: <p className='items-container__message'>No item found</p>
			}
		</>

	)
}

export default ItemsContainer;