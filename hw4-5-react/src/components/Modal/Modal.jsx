import React from 'react';
import './Modal.scss'

const Modal = (props) => {
	const {backgroundColor,headerColor,header, text, actions, handleClose} = props
	return (
		<div className='modal__background'
			 onClick={(event) => event.currentTarget === event.target && handleClose()}>
			<div style={{background: backgroundColor}} className='modal'>
				<div style={{background: headerColor}} className='modal__header'>
					<h5>{header}</h5>
					<div className='modal__close' onClick={handleClose}/>
				</div>
				<div className='modal__content'>
					<p className={"modal__text"}>{text}</p>
				</div>
				<div className='modal__actions'>
					{actions}
				</div>
			</div>
		</div>
	)
}

export default Modal;