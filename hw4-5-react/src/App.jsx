import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {itemsLoading} from "./store/action/ItemsActions";
import {cartLoading} from "./store/action/CartActions";
import {loadFavorites} from "./store/action/FavoritesActions";
import AppRouter from "./routing/AppRouter";
import Header from "./components/Header/Header";

const App = () => {

	const dispatch = useDispatch()
	const modal = useSelector(store => store.modal)

	useEffect(() => {
		dispatch(itemsLoading())
		dispatch(cartLoading())
		dispatch(loadFavorites())
	}, [dispatch])

	return (
		<div>
			<Header/>
			<AppRouter/>
			{modal}
		</div>
	)
}

export default App