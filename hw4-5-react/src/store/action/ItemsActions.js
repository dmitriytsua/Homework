import axios from 'axios'

export const ITEMS_LOADED = 'ITEMS_LOADED'

export const itemsLoading = () => dispatch => {
	axios.get('./items.json')
		.then(res => dispatch({type: ITEMS_LOADED, items: res.data}))
}