import {fromLocalStorage, toLocalStorage} from "../../utils/LocalStorage";

export const CART_LOADING = 'CART_LOADING'
export const ADD_TO_CART = 'ADD_TO_CART'
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART'
export const INCR_COUNT_IN_CART = 'INCR_COUNT_IN_CART'
export const DECR_COUNT_IN_CART = 'DECR_COUNT_IN_CART'
export const CLEAN_CART = 'CLEAN_CART'

export const cartLoading = () => dispatch => {
	const cart = fromLocalStorage('cart', [])
	dispatch({type: CART_LOADING, cart})
}

export const addToCart = item => dispatch => {
	const cart = fromLocalStorage('cart', [])
	cart.push(item)
	toLocalStorage('cart', cart)
	dispatch({type: ADD_TO_CART, cart})
}

export const removeFromCart = item => dispatch => {
	let cart = fromLocalStorage('cart', [])
	cart = cart.filter(i => i.article !== item.article)
	toLocalStorage('cart', cart)
	dispatch({type: REMOVE_FROM_CART, cart})
}

export const incrementItemCountInCart = item => dispatch => {
	const cart = fromLocalStorage('cart', [])
	cart.push(item)
	toLocalStorage('cart', cart)
	dispatch({type: INCR_COUNT_IN_CART, cart})
}

export const decrementItemCountInCart = item => dispatch => {
	const cart = fromLocalStorage('cart', [])
	const find = cart.find(i => i.article === item.article)
	cart.splice(cart.indexOf(find), 1)
	toLocalStorage('cart', cart)
	dispatch({type: DECR_COUNT_IN_CART, cart})
}

export const cleanCart = () => dispatch => {
	toLocalStorage('cart', [])
	dispatch({type: CLEAN_CART})
}
