import {CLOSE_MODAL, OPEN_MODAL} from "../action/ModalActions";
import initStore from "../InitStore";

const modalReducer = (store = initStore.modal, {type, modal}) => {
	switch (type) {
		case OPEN_MODAL:
			return modal
		case CLOSE_MODAL:
			return null
		default:
			return store
	}
}

export default modalReducer