import React from 'react';
import {useSelector} from "react-redux";
import ItemsContainer from "../components/ItemsContainer/ItemsContainer";

const MainPage = () => {

	const items = useSelector(store => store.items)

	return (
		<main>
			<div className="container">
				<ItemsContainer items={items}/>
			</div>
		</main>
	)
}

export default MainPage