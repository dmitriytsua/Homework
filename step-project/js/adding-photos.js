$(function() {

    let selectedClass = "";
    let gallery = $('.gallery');
    let loadMore = $('.load-more');

    $(".changing").click(function(){
        $(".changing").removeClass("list-active");
        $(this).addClass("list-active");
        selectedClass = $(this).attr("data-rel");
        $(gallery).fadeTo(100, 0.1);
        $(".gallery div").not("."+selectedClass).fadeOut().removeClass('scaling');
        setTimeout(function() {
            $("."+selectedClass).fadeIn().addClass('scaling');
            $(gallery).fadeTo(300, 1);
        }, 300);
        if($("."+selectedClass).length > 12){
            $(loadMore).html("<i class='fas fa-plus'></i>Load More").removeClass("hide-button");
            $(gallery).css('max-height','640px');
        }else {
            $(loadMore).addClass("hide-button");
        }
    });

    $(loadMore).click(function() {
        let maxHeight = $(gallery).css('max-height');
        $(gallery).css('max-height', "+=620px");

        if (maxHeight === "1260px") {
            $(loadMore).addClass("hide-button");
        }
    });
});







$('.gallery-img').hover(function() {
        if ($(this).hasClass('Graphic')) {
            $(this).append($("<div class='img-hover'><div class='img-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-square'></i></a></div><div class = 'portfolio-info'><p class='work-hover-text'>AMAZING GRAPHIC</p><p class='work-hover-text' style='color: #8A8A8A; font-weight: 300; font-size: 12px' >Graphic Design</p></div></div>"));
        }
        if ($(this).hasClass('Web')) {
            $(this).append($("<div class='img-hover'><div class='img-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-square'></i></a></div><div class = 'portfolio-info'><p class='work-hover-text'>CREATIVE DESIGN</p><p class='work-hover-text' style='color: #8A8A8A; font-weight: 300; font-size: 12px'>Web Design</p></div></div>"));
        }
        if ($(this).hasClass('Wordpress')) {
            $(this).append($("<div class='img-hover'><div class='img-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-square'></i></a></div><div class = 'portfolio-info'><p class='work-hover-text'>OUR WORDPRESS</p><p class='work-hover-text' style='color: #8A8A8A; font-weight: 300; font-size: 12px'>Wordpress</p></div></div>"));
        }
        if ($(this).hasClass('Landing')) {
            $(this).append($("<div class='img-hover'><div class='img-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-square'></i></a></div><div class = 'portfolio-info'><p class='work-hover-text'>LANDING PAGES</p><p class='work-hover-text' style='color: #8A8A8A; font-weight: 300; font-size: 12px'>Landing Pages</p></div></div>"));
        }
        $('.img-hover').animate({
            bottom: 0,
        },600)
    },
    function() {
        $(this).find(".img-hover").remove();
    }
);

