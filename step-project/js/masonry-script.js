$(".grid-item").slice(0, 8).css('display', 'inline-block').show();
$(".load-more-masonry").on("click", function(e){
    $(".container-loader").css("display", "block");
    setTimeout(function() {
        e.preventDefault();
        $(".grid-item:hidden").slice(0, 4).slideDown();
        $('.grid').masonry({
            itemSelector: '.grid-item',
            columnWidth: 370,
            isFitWidth: true,
            gutter: 20,
        });

        if($(".grid-item:hidden").length === 0) {
            $(".load-more-masonry").addClass("hide-button");
        }
        $(".container-loader").css("display", "none");
    }, 5000);
});

$('.grid').masonry({
    itemSelector: '.grid-item',
    columnWidth: 370,
    isFitWidth: true,
    gutter: 20,
});
