function showList(list){
    function arrayUL(root, arr){
        let ul=document.createElement('ul');
        let li;
        root.appendChild(ul);
        arr.forEach(function(item){if (Array.isArray(item)){arrayUL(li,item);
        return;}
        li = document.createElement('li');
        li.appendChild(document.createTextNode(item));
        ul.appendChild(li);
        });}
    let div=document.getElementById('list');
    arrayUL(div,list);
    let time=3;
    let timerID=setInterval(function(){document.getElementById("timer").innerText=time;
        if (time===0) {document.getElementById('list').classList.add("none");
        document.getElementById('timer').classList.add("none");
        clearInterval(timerID);
        list=[];}
        time--;
    },1000);}
showList(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]);