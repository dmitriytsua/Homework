import React, {Component} from 'react';
import Button from "./content/Button/Button";
import './App.scss';
import Modal from "./content/Modal/Modal";

class App extends Component {
    state = {
        modal: null
    }

    setModal(modalType) {
        switch (modalType) {
            case 'confirm':
                this.setState({
                    modal: <Modal
                        backgroundColor={"#e74c3c"} headerColor={"#d44637"}
                        header={'Do you want to delete this file?'}
                        text={'Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?'}
                        handleClose={() => this.setModal(null)}
                        actions={
                            <>
                                <Button backgroundColor={'#b3382c'} text={'Ok'} onClick={() => this.setModal(null)}/>
                                <Button backgroundColor={'#b3382c'} text={'Cancel'} onClick={() => this.setModal(null)}/>
                            </>
                        }
                    />
                });
                break
            case 'info':
                this.setState({
                    modal: <Modal backgroundColor={"#626262"} headerColor={"#a982b9"}
                        header={'Second modal, bla-bla-bla'}
                        text={'Bla-bla-bla, yeah, bla-bla and etc'}
                        closeButton={false}
                        handleClose={() => this.setModal(null)}
                        actions={<div>
                            <Button backgroundColor={'#a982b9'} text={'Bla!'} onClick={() => this.setModal(null)}/>
                            <Button backgroundColor={'orange'} text={'Bla?'} onClick={() => this.setModal(null)}/>
                        </div>
                        }
                    />
                });
                break
            default:
                this.setState({
                    modal: null
                })
        }
    }

    render() {
        const {modal} = this.state
        return (
            <div>
                <div className={"buttons"}>
                    <Button backgroundColor={"red"} text={'Open first modal'} onClick={() => this.setModal('confirm')}/>
                    <Button backgroundColor={"gray"} text={'Open second modal'} onClick={() => this.setModal('info')}/>
                </div>
                {modal}
            </div>
        )
    }
}

export default App;
