let interval;
let timeCounter;
let timer;
let images = ["./img/2.jpg", "./img/3.jpg", "./img/4.png", "./img/1.jpg"], x = -1;
function counter (){let timeLeft = 3;
timer = setInterval(function f(){
    document.getElementById("timer").innerText = timeLeft;
    timeLeft -= 1;
    if(timeLeft <= 0)
        clearInterval(timer);},1000);}
function displayNextImage(){
    x = (x === images.length - 1) ? 0 : x + 1;
    document.getElementById("img").src = images[x];}
interval = setInterval(displayNextImage, 4000);
timeCounter = setInterval(counter, 4000);
document.getElementById('stop').addEventListener('click', function () {
    clearInterval(interval);
    clearInterval(timeCounter);
    clearInterval(timer);});
document.getElementById('resume').addEventListener('click', function () {
    interval = setInterval(displayNextImage, 4000);
    timeCounter = setInterval(counter, 4000);
});