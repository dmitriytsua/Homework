import React, {Component} from 'react';
import Header from "./content/Header/Header";
import Container from "./content/Container/Container";
import Modal from "./content/Modal/Modal";
import Button from "./content/Button/Button";
import axios from 'axios'
import './App.scss';

class App extends Component {
	state = {
		items: [],
		cart: [],
		modal: null
	}

	componentDidMount() {
		axios.get('../items.json')
			.then(res => this.setState({items: res.data}))
		this.setState({
			cart: JSON.parse(localStorage.getItem('cart')) || []
		})
	}

	addToCart(item) {
		const {cart} = this.state
		cart.push(item)
		localStorage.setItem('cart', JSON.stringify(cart))
		this.setState({cart, modal: null})
	}

	handleModal(item) {
		this.setState({
			modal: <Modal
				backgroundColor={"gray"}
				headerColor={"lightgray"}
				header='Are you sure?'
				text='Do you want add this item to the cart?'
				closeButton={true}
				actions={<div>
					<Button backgroundColor={"forestgreen"} text='Add' onClick={() => this.addToCart(item)}/>
					<Button backgroundColor={"red"} text='Cancel' onClick={() => this.setState({modal: null})}/>
				</div>}
				handleClose={() => this.setState({modal: null})}
			/>
		})
	}

	render() {
		const {items, cart, modal} = this.state
		return (
			<div className="App">
				<Header cart={cart}/>
					<Container items={items} addToCart={(item) => this.handleModal(item)}/>
				{modal}
			</div>
		);
	}
}

export default App;
