import React, {Component} from 'react';
import Cart from "../Cart/Cart";
import './Header.scss'

class Header extends Component {
	render() {
		const {cart} = this.props
		return (
			<header>
				<div>
					<nav className='nav'>
						<a className={"nav__text"} href={"/"}>Bad store</a>
						<img alt={'logo'} className='logo' src={"./logo192.png"} />
						<Cart cart={cart}/>
					</nav>
				</div>
			</header>
		);
	}
}

export default Header;