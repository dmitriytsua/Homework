import React, {Component} from 'react';
import PropTypes from 'prop-types'
import Button from "../Button/Button";
import './Modal.scss'

class Modal extends Component {
	static propTypes = {
		header: PropTypes.string,
		text: PropTypes.string,
		closeButton: PropTypes.bool,
		actions: PropTypes.object,
		backgroundColor: PropTypes.string,
		handleClose: PropTypes.func,
		headerColor: PropTypes.string
	}

	static defaultProps = {
		header: 'Title',
		text: 'Modal text',
		closeButton: true,
		backgroundColor: '#fff',
		headerColor: "#fff",
		actions: <Button text={'Ok'} onClick={() => {
		}}/>,
		handleClose: () => {
		}
	}

	render() {
		const {backgroundColor,headerColor,header, text, actions, handleClose} = this.props
		return (
			<div className='modal__background'
				 onClick={(event) => event.currentTarget === event.target && handleClose()}>
				<div style={{background: backgroundColor}} className='modal'>
					<div style={{background: headerColor}} className='modal__header'>
						<h5>{header}</h5>
						<div className='modal__close' onClick={handleClose}/>
					</div>
					<div className='modal__content'>
						<p className={"modal__text"}>{text}</p>
					</div>
					<div className='modal__actions'>
						{actions}
					</div>
				</div>
			</div>
		);
	}
}

export default Modal;