import React, {Component} from 'react';
import Item from "../Item/Item";
import './Container.scss'

class Container extends Component {

	render() {
		const {items, addToCart} = this.props
		const renderItems = items.map(item => <Item key={item.article} item={item} addToCart={addToCart}/>)
		return (
			<div className='container'>
				<div className='container__items'>
					{renderItems}
				</div>
			</div>
		);
	}
}

export default Container;