import React, {useEffect, useState} from 'react';
import CartItem from "../CartItem/CartItem";
import "./CartContainer.scss"

const CartContainer = (props) => {

	const {
		items,
		removeFromCart,
		handleFavorites,
		incrementItemCountInCart,
		decrementItemCountInCart
	} = props

	const [totalPrice, setTotalPrice] = useState()

	useEffect(() => {
		if (items.length > 0) {
			const tPrice = items.map(item => item.price).reduce((p,v) => p = p + v)
			setTotalPrice(tPrice)
		} else {
			setTotalPrice(0)
		}
	}, [items])

	const uniqueArticles = items.map(item => item.article).filter((it, inx, arr) => arr.indexOf(it) === inx)

	const itemCards = uniqueArticles.map((article, index) => {
		const itemsByArticle = items.filter(i => i.article === article)
		const item = itemsByArticle[0]
		return <CartItem key={(index + 1) * item.id * Math.random()}
		                 item={item}
		                 count={itemsByArticle.length}
		                 removeFromCart={removeFromCart}
		                 handleFavorites={handleFavorites}
		                 incrementItemCountInCart={incrementItemCountInCart}
		                 decrementItemCountInCart={decrementItemCountInCart}/>
	})

	return (
		<div className='cart-container'>
			{itemCards.length > 0
				? itemCards
				: <p className='cart-container__message'>Cart is empty</p>
			}
			<p className='cart-total-price'>{`Price: ${totalPrice} $;`}</p>
		</div>
	)
}

export default CartContainer;