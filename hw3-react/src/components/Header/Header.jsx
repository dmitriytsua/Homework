import React from 'react';
import {Link} from "react-router-dom";
import './Header.scss'
import {cartIcon} from "../../assets/AppIcons";

const Header = (props) => {

	const {cart = []} = props

	return (
		<header>
			<div className='container'>
				<nav className='navbar'>
					<a className='navbar__logo '  href="/">Very Bad Shop</a>
					<menu className='navbar__menu'>
						<li className='navbar__menu'>
							<Link className={"navbar__menu--link"} to='/'>Main</Link>
						</li>
						<li className='navbar__menu'>
							<Link className={"navbar__menu--link"} to='/favorites'>Favorites</Link>
						</li>
						<li className='navbar__menu'>
							<Link className={"navbar__menu--link"} to='/cart'>
									Cart
									{cartIcon()}
									{cart.length > 0 && <span className='navbar__menu--badge'>{cart.length}</span>}
							</Link>
						</li>
					</menu>
				</nav>
			</div>
		</header>
	);
}

export default Header;