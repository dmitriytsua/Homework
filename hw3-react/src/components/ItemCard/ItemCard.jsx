import React, {useEffect, useState} from 'react';
import Button from "../Button/Button";
import {starIcon} from "../../assets/AppIcons";
import {fromLocalStorage} from "../../utils/Utils";
import "./ItemCard.scss"

const ItemCard = (props) => {

	const {item, addToCart, handleFavorites} = props
	const {img, title, article, color, price} = item
	const [isFavorite, setIsFavorite] = useState(false)

	useEffect(() => {
		const favs = fromLocalStorage('favorites', [])
		setIsFavorite(favs.some(itm => itm.article === article))
	}, [])

	return (
		<div className='card'>
			<div className='card__img-wrapper'>
				<img className='card__img' src={img} alt={title}/>
			</div>
			<div className='card__content'>
				<h5 className='card__title'>{title}</h5>
				<p className='card__article'>Article: {article}</p>
				<p className='card__color'>Color: {color}</p>
			</div>
			<div className='card__footer'>
				<span className='card__price'>{price} $</span>
				<div className='card__actions'>
					<Button backgroundColor={"green"} text='Add to cart' onClick={() => addToCart(item)}/>
					<span className='card__actions__fav' onClick={() => {
						const isFav = handleFavorites(item)
						setIsFavorite(isFav)
					}}>
                        {starIcon(isFavorite ? '#ffc107' : '#ffffff')}
                    </span>
				</div>
			</div>
		</div>
	)
}

export default ItemCard;