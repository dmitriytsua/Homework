export const toLocalStorage = (key, value) => {
	localStorage.setItem(key, JSON.stringify(value))
}

export const fromLocalStorage = (key, init) => {
	return JSON.parse(localStorage.getItem(key)) || init
}