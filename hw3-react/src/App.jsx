import React, {useEffect, useState} from 'react';
import AppRouter from "./routing/AppRouter";
import Header from "./components/Header/Header";
import axios from "axios"
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";
import {fromLocalStorage, toLocalStorage} from "./utils/Utils";

const App = () => {

	const [items, setItems] = useState([])
	const [favorites, setFavorites] = useState([])
	const [cart, setCart] = useState([])
	const [modal, setModal] = useState(null)

	useEffect(() => {
		axios.get('./items.json')
			.then(res => setItems(res.data))
		setFavorites(fromLocalStorage('favorites', []))
		setCart(fromLocalStorage('cart', []))
	}, [])

	const addToCart = (item) => {
		setModal(<Modal
			backgroundColor={"#4d4d4d"}
			headerColor={"#4d4949"}
			header='Add to cart?'
			text='Do you want add this item to the cart?'
			closeButton={false}
			actions={<div>
				<Button backgroundColor={"#407c49"} text='Submit' onClick={() => {
					const c = cart.concat([item])
					toLocalStorage('cart', c)
					setCart(c)
					setModal(null)

				}}/>
				<Button backgroundColor={"#f22e21"} text='Cancel' onClick={() => setModal(null)}/>
			</div>}
			handleClose={() => setModal(null)}
		/>)
	}

	const removeFromCart = (item) => {
		setModal(<Modal
			backgroundColor={"#e34234"}
			headerColor={"#e34234"}
			header='Remove from cart'
			text='Do you want remove this item from the cart?'
			closeButton={false}
			actions={<div>
				<Button backgroundColor={"#407c49"} text='Submit' onClick={() => {
					const cart = fromLocalStorage('cart', [])
					const removed = cart.filter(itm => itm.article !== item.article)
					toLocalStorage('cart', removed)
					setCart(removed)
					setModal(null)
				}}/>
				<Button backgroundColor={"gray"} text='Cancel' onClick={() => setModal(null)}/>
			</div>}
			handleClose={() => setModal(null)}
		/>)
	}

	const incrementItemCountInCart = (item) => {
		const cart = fromLocalStorage('cart', [])
		cart.push(item)
		toLocalStorage('cart', cart)
		setCart(cart)
	}

	const decrementItemCountInCart = (item) => {
		const cart = fromLocalStorage('cart', [])
		cart.splice(cart.indexOf(item), 1)
		toLocalStorage('cart', cart)
		setCart(cart)
	}

	const handleFavorites = (item) => {
		const favs = fromLocalStorage('favorites', [])
		if (favs.some(itm => itm.article === item.article)) {
			const removed = favs.filter(itm => itm.article !== item.article)
			toLocalStorage('favorites', removed)
			setFavorites(removed)
			return false
		} else {
			const added = favs.concat([item])
			toLocalStorage('favorites', added)
			setFavorites(added)
			return true
		}
	}

	return (
		<div>
			<Header cart={cart}/>
			<AppRouter appState={{items, favorites, cart}}
			           addToCart={addToCart}
			           removeFromCart={removeFromCart}
			           handleFavorites={handleFavorites}
			           incrementItemCountInCart={incrementItemCountInCart}
			           decrementItemCountInCart={decrementItemCountInCart}
			/>
			{modal}
		</div>
	);
};

export default App;