import React from 'react';
import ItemsContainer from "../components/ItemsContainer/ItemsContainer";

const FavoritesPage = (props) => {

	const {items, handleFavorites, addToCart} = props

	return (
		<main>
			<div className="container">
				<ItemsContainer items={items} handleFavorites={handleFavorites} addToCart={addToCart}/>
			</div>
		</main>
	)
}

export default FavoritesPage;