import React from 'react';
import CartContainer from "../components/CartContainer/CartContainer";

const CartPage = (props) => {

	const {
		items,
		removeFromCart,
		addToCart,
		handleFavorites,
		incrementItemCountInCart,
		decrementItemCountInCart
	} = props

	return (
		<div>
			<div className="container">
				<CartContainer items={items}
				               removeFromCart={removeFromCart}
				               addToCart={addToCart}
				               handleFavorites={handleFavorites}
				               incrementItemCountInCart={incrementItemCountInCart}
				               decrementItemCountInCart={decrementItemCountInCart}/>
			</div>
		</div>
	)
}

export default CartPage;