import React from 'react';
import {Route, Switch} from "react-router-dom";
import MainPage from "../pages/MainPage";
import FavoritesPage from "../pages/FavoritesPage";
import CartPage from "../pages/CartPage";

const AppRouter = (props) => {

	const {
		appState: {items, cart, favorites},
		addToCart,
		removeFromCart,
		handleFavorites,
		incrementItemCountInCart,
		decrementItemCountInCart
	} = props

	return (
		<Switch>
			<Route path='/' exact
			       render={() => <MainPage items={items}
			                               addToCart={addToCart}
			                               handleFavorites={handleFavorites}/>}
			/>
			<Route path='/favorites' exact
			       render={() => <FavoritesPage items={favorites}
			                                    addToCart={addToCart}
			                                    handleFavorites={handleFavorites}/>}
			/>
			<Route path='/cart' exact
			       render={() => <CartPage items={cart}
			                               addToCart={addToCart}
			                               removeFromCart={removeFromCart}
			                               handleFavorites={handleFavorites}
			                               incrementItemCountInCart={incrementItemCountInCart}
			                               decrementItemCountInCart={decrementItemCountInCart}/>}
			/>
		</Switch>
	);
};

export default AppRouter;