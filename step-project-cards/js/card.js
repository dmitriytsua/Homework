import {DragDrop} from "./DragDrop.js";

export class Card {
    constructor(cards) {
        this.id = cards.id;
        this.cards = cards.content;

        this.elements = {
            cardContainer: document.createElement("div"),
            detailsBtn: document.createElement('button'),
            deleteBtn: document.createElement('button'),
            doctorDetailsFields: document.createElement('div'),
            editCardContainer: document.createElement("div")
        }
    }

    render() {
        this.elements.cardContainer.classList.add('card');
        this.elements.detailsBtn.classList.add('details-btn');
        this.elements.deleteBtn.classList.add('delete-btn');
        this.elements.doctorDetailsFields.classList.add('doctorDetails');
        this.elements.editCardContainer.classList.add('editCardContainer');
        this.elements.detailsBtn.innerText = 'Показать';
        this.elements.deleteBtn.innerText = 'X';

        const cardContentInfo = Object.entries(this.cards);
        cardContentInfo.forEach(([key, value]) => {

            if (key.toLowerCase() === "name"||key.toLowerCase() === "purpose"
                ||key.toLowerCase() === "medic") {

                let mainCardField = document.createElement("p");
                mainCardField.classList.add("card-fields");
                mainCardField.innerText = `${key.toUpperCase()}: ${value}`;
                this.elements.cardContainer.append(mainCardField);
            } else {
                let detailsCardField = document.createElement("p");
                detailsCardField.classList.add("card-fields");
                detailsCardField.innerText = `${key.toUpperCase()}: ${value}`;
                this.elements.doctorDetailsFields.append(detailsCardField);
            }
        })

        this.elements.cardContainer.append( this.elements.deleteBtn);
        this.elements.cardContainer.insertAdjacentElement('beforeend', this.elements.detailsBtn);
        document.querySelector('.task-container').append(this.elements.cardContainer);

        this.elements.deleteBtn.addEventListener('click', () => this.deleteCardRequest());
        this.elements.detailsBtn.addEventListener('click', () => {
            if (!document.querySelector('.doctorDetails')) {
                this.elements.cardContainer.append(this.elements.doctorDetailsFields);
                this.elements.detailsBtn.innerText = 'Скрыть';
            } else {
                this.elements.detailsBtn.innerText = 'Показать';
                document.querySelector('.doctorDetails').remove();
            }
        })
        DragDrop('.card');
    }

    deleteCardRequest() {
        fetch(`https://ajax.test-danit.com/api/cards/${this.id}`, {
            method: 'DELETE',
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                'Authorization': `Bearer ${sessionStorage.getItem('token')}`
            },
        }).then((response) => {
            if (response.status >= 200 && response.status < 300) {
                return response;
            } else {
                let error = new Error(response.statusText);
                error.response = response;
                throw error
            }
        }).then(response => {
                if (response.status >= 200 && response.status < 300) {
                    this.elements.cardContainer.remove();
                    this.elements.deleteBtn.removeEventListener('click', () => this.deleteCardRequest());
                }
            })
    }
}