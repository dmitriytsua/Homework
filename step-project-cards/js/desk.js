import {Card} from "./card.js";

export class TaskDesk {
    constructor() {
        this.cards = [];
        this.elements = {
            desk: document.createElement("div"),
            taskContainer: document.createElement("div"),
            filterContainer: document.createElement("div"),
            urgencySelect: document.createElement('select'),
        }
    }
    init() {
        this.getCards();
        this.renderTaskDesk();
    }
    renderTaskDesk() {
        this.elements.desk.classList.add('task-desk');
        this.elements.taskContainer.classList.add('task-container');

        const statuses = ['Все', 'Открытый', 'Завершен'];
        const urgency = ['Все', 'Низкая', 'Средняя', 'Высокая'];
        statuses.forEach(e => {
            const statusOption = document.createElement('option');
            statusOption.innerText = e;
        })
        urgency.forEach(e => {
            const urgencyOption = document.createElement('option');
            urgencyOption.innerText = e;
            this.elements.urgencySelect.append(urgencyOption);
            this.elements.urgencySelect.classList.add('select-desk');
        })
        this.elements.desk.append( this.elements.taskContainer);
        document.querySelector(".wrapper").append(this.elements.desk);
    }
    async getCards() {
        await fetch('https://ajax.test-danit.com/api/cards', {
            method: 'GET',
            headers: {"Content-type": "application/json; charset=UTF-8",
                'Authorization': `Bearer ${sessionStorage.getItem('token')}`
            },
        })
            .then((response) => {
                if (response.status >= 200 && response.status < 300) {
                    return response;
                } else {
                    let error = new Error(response.statusText);
                    error.response = response;
                    throw error;
                }
            })
            .then(response=> response.json())
            .then(cards => {
                 if (cards.length > 0) {
                    cards.forEach(cardElem => {
                        this.cards.push(cardElem)
                    })
                    this.cards.forEach(e => new Card(e).render());
                }
            })
    }
    clearCards() {
        document.querySelectorAll('.card').forEach(e => e.remove())
    }

}

