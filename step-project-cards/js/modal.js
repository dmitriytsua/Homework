import {TaskDesk} from "./desk.js";
const token = sessionStorage.getItem('token');
const url = 'https://ajax.test-danit.com/api/cards/login'
export class Modal {
  constructor(email, password, submitBtn) {
    this.form = document.createElement('form');
    this.closeBtn = document.createElement('button');
    this.formBody = document.createElement('div');
    this.email = document.createElement('input');
    this.password = document.createElement('input');
    this.submitBtn = document.createElement('button');
  }
  changeBtn() {
    document.querySelector('.header__login-button').style.display = "none";
    document.querySelector('.header__create-button').style.display = "block";
  }
  closeSigningForm() {
    document.body.addEventListener('click', (event) => {
      if (event.target.classList.value === ('signIn')) {
        this.form.style.display = 'none';
      }
    })
  }

  render() {
    this.form.className = "signIn"
    this.closeBtn.className = "signIn__closeBtn";
    this.formBody.className = "signIn__body";
    this.email.className = "signIn__email";
    this.password.className = "signIn__password";
    this.submitBtn.className = "signIn__submitBtn";

    this.closeBtn.textContent = "Закрыть";
    this.submitBtn.textContent = "Подтвердить";

    this.email.setAttribute("type", "email");
    this.password.setAttribute("type", "password");
    this.submitBtn.setAttribute("type", "submit");

    this.email.name = 'email';
    this.password.name = 'password';

    this.email.placeholder = "Введите почту";
    this.password.placeholder = "Введите пароль";

    this.formBody.append(this.closeBtn, this.email, this.password, this.submitBtn);
    this.form.append(this.formBody);
    document.body.prepend(this.form);


    this.submitBtn.addEventListener('click', (event) => {
      event.preventDefault();
      this.getLogin(this.email, this.password, this.form);
      document.querySelector('.signIn').remove();
    })

    this.closeBtn.addEventListener('click', (event) => {
      event.preventDefault();
      this.form.style.display = "none";
    })
    this.closeSigningForm();
  }


    getLogin = async (email, password, form) => {
        const response = await fetch(url, {
            method: 'post',
            body: JSON.stringify({
                email: this.email.value,
                password: this.password.value
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },

        })
        if (response.status >= 200 && response.status < 300) {
            const answer = await response.text()
            sessionStorage.setItem('token', answer);
            form.style.display = 'none';
            this.changeBtn();
            const bord = new TaskDesk().init();
        }else {alert('Неверный логин или пароль')}
    }
}
    export {token};