import {VisitCardiologist, VisitDentist, VisitTherapist} from './visit.js';

export class  Form {
    constructor() {
        this.elements = {
            select: document.createElement('select'),
            disabledOption: document.createElement('option'),
            cardiologist: document.createElement('option'),
            dentist: document.createElement('option'),
            therapist: document.createElement('option'),
            formDiv: document.createElement('div')
        }
    }
    clearDiv(el) {
        el = this.elements.formDiv
        el.innerHTML = ''
    }
    createForm(event) {
        const container = document.querySelector('.wrapper'),
            showCardiologist = new VisitCardiologist(),
            showDentist = new VisitDentist(),
            showTherapist = new VisitTherapist();

        container.append(this.elements.formDiv)
        if (event.value === 'Кардиолог') {
            this.clearDiv()
            showCardiologist.render(this.elements.formDiv)
        } else if (event.value === 'Дантист') {
            this.clearDiv()
            showDentist.render(this.elements.formDiv)
        } else {
            this.clearDiv()
            showTherapist.render(this.elements.formDiv)
        }
    }

    render() {
        const {disabledOption, cardiologist, dentist, therapist, select} = this.elements
        disabledOption.textContent = 'Выберите санитара:';
        disabledOption.disabled = true;
        disabledOption.selected = true;
        cardiologist.textContent = 'Кардиолог';
        dentist.textContent = 'Дантист';
        therapist.textContent = 'Терапевт';

        select.append(disabledOption, cardiologist, dentist, therapist);
        select.addEventListener('change', (e) => {
            let event = e.target;
            this.createForm(event);
        })
        document.querySelector('.wrapper').append(select);
        select.classList.add('doctor-select');
    }
}